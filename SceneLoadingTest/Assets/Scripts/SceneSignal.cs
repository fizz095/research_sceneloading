﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSignal : MonoBehaviour
{
    private string myName;
    private Object cachedGameObject;

    private void Awake()
    {
        myName = gameObject.scene.name;
        cachedGameObject = gameObject;

        Debug.Log("AWAKE: " + myName);

        SceneManager.activeSceneChanged += (from, to) => 
        {
            if (to == gameObject.scene)
            {
                Debug.Log("ACTIVE: " + gameObject.scene.name);
            }
        };        
    }

    private void Start()
    {
        Debug.Log("START: " + myName);
    }

    private void Update()
    {
        if (string.IsNullOrEmpty(myName))
        {
            Debug.LogError("UPDATE - NAME IS NULL (wtf)");
        }

        if (cachedGameObject == null)
        {
            Debug.LogError("UPDATE - CACHED OBJECT IS NULL: " + myName);
        }

        if (gameObject == null)
        {
            Debug.LogError("UPDATE - OBJECT IS NULL: " + myName);
        }

        if (cachedGameObject != gameObject)
        {
            Debug.LogError("UPDATE - CACHED OBJECT IS OBSOLETE: " + myName);
        }
    }
}
