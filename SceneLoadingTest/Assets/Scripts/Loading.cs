﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Loading : MonoBehaviour
{
    private const int SCENES_TOTAL = 2;
    private static readonly int[] SCENE_BUILD_INDICES = new[] { 1, 2 };

    private int managedSceneIndex;
    private bool initialLoadingSuccess;

    private bool ActivateManagedScene()
    {
        return SceneManager.SetActiveScene(SceneManager.GetSceneByBuildIndex(SCENE_BUILD_INDICES[managedSceneIndex]));
    }

    private IEnumerator Start()
    {
        var asyncLoading = new AsyncOperation[SCENES_TOTAL];

        for (int i = 0; i < SCENES_TOTAL; i++)
        {
            asyncLoading[i] = SceneManager.LoadSceneAsync(SCENE_BUILD_INDICES[i], LoadSceneMode.Additive);
        }

        for (int i = 0; i < SCENES_TOTAL; i++)
        {
            yield return asyncLoading[i];
        }

        managedSceneIndex = 0;
        initialLoadingSuccess = ActivateManagedScene();
    }

    private void Update()
    {
        if (!initialLoadingSuccess)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow) && managedSceneIndex > 0)
        {
            managedSceneIndex--;
            ActivateManagedScene();
        }

        if (Input.GetKeyDown(KeyCode.RightArrow) && managedSceneIndex < SCENES_TOTAL - 1)
        {
            managedSceneIndex++;
            ActivateManagedScene();
        }
    }
}
